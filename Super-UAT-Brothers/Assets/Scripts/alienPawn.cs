﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class alienPawn : pawn {

	private Rigidbody2D rb;
	private SpriteRenderer sr;
	private Transform tf;
	private Animator anim;

	public float moveSpeed;
	public float jumpForce;
	public bool isGrounded;
	public float groundDistance = 0.1f;

	public int maxAirJumps;
	public int currentAirJumps;

	public AudioClip jumpSound;

	public void Start() {
		rb = GetComponent<Rigidbody2D>();
		sr = GetComponent<SpriteRenderer> ();
		tf = GetComponent<Transform> ();
		anim = GetComponent<Animator> ();
	}

	public void Update() {
		//Check if player is grounded
		CheckForGrounded();
	}

	public void CheckForGrounded() {
		RaycastHit2D hitInfo = Physics2D.Raycast (tf.position, Vector3.down, groundDistance);
		if (hitInfo.collider == null) {
			isGrounded = false;
			anim.Play ("AlienJump");
		} else {
			isGrounded = true;
			currentAirJumps = maxAirJumps; // Resets the currentAirJumps to 1 after the player lands
		}
	}

	public override void Move(float xMovement) {
		rb.velocity = new Vector2 (xMovement * moveSpeed, rb.velocity.y);

		// If velocity is <0, flip sprite
		if (isGrounded && rb.velocity.x < 0) {
			anim.Play ("AlienWalk");
			sr.flipX = true;
		} else if (isGrounded && rb.velocity.x > 0) {
			anim.Play ("AlienWalk");
			sr.flipX = false;
		} else if (isGrounded) {
			anim.Play ("AlienIdle");
		} else if (rb.velocity.x < 0) {
			sr.flipX = true;
		} else if (rb.velocity.x > 0) {
			sr.flipX = false;
		}
	}

	public override void Jump() {
		if (isGrounded) {
			rb.AddForce (Vector2.up * jumpForce, ForceMode2D.Force);
			anim.Play ("AlienJump");
			AudioSource.PlayClipAtPoint (jumpSound, GetComponent<Transform> ().position, 1.0f); 
		} else if (currentAirJumps > 0) { // States that if they are in the air and have currentAirJumps left, they can jump in the air
			currentAirJumps -= 1;
			rb.AddForce (Vector2.up * jumpForce, ForceMode2D.Force);
			anim.Play ("AlienJump");
			AudioSource.PlayClipAtPoint (jumpSound, GetComponent<Transform> ().position, 1.0f); 
		}
	}
}
