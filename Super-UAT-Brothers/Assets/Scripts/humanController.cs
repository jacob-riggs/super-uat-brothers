﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class humanController : controller {

	public KeyCode jumpKey = KeyCode.Space;
	public KeyCode rightKey = KeyCode.D;
	public KeyCode leftKey = KeyCode.A;
	public KeyCode upKey = KeyCode.UpArrow;
	public KeyCode downKey = KeyCode.DownArrow;

	// Use this for initialization
	void Start () {
		//rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {

		float xMovement = Input.GetAxis ("Horizontal"); // Gets the X-Axis input
	
		pawn.Move (xMovement); // Sends xMovement to Move in pawn

		// Jump
		if (Input.GetKeyDown (jumpKey)) {
			pawn.Jump ();
		}
	}
}
