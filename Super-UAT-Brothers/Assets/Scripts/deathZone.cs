﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathZone : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other){ // Destroys the player if they fall
		if (other.tag == "Player") {
			Destroy (other.gameObject);
		}
	}
}
