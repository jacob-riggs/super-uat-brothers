﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour {


	public static gameManager instance;
	public static gameManager spawn;

	//public humanController playerOne;
	//public GameObject defaultPlayerPawn;
	Vector3 FirstSpawn = new Vector3(-8.04f, -0.63f, 0f);
	public Vector3 CurrentSpawn;
	public GameObject playerPrefab;
	public int maxLives;
	public int currentLives;

	// Use this for initialization
	void Start () {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
		spawn = this; // Will allow checkpointChecker to access the currentSpawn
		CurrentSpawn = FirstSpawn; // Sets the current spawn to the beginning of the map
	}
	
	// Update is called once per frame
	void Update () {
		spawnPlayer ();	// Calls the spawnPlayer function
	}

	void spawnPlayer() {
		if (currentLives != 0) { // Checks to see if the player has lives
			if (GameObject.FindGameObjectWithTag ("Player") == null) { // Checks for player
				currentLives--; // Subtracts from lives
				Instantiate (playerPrefab, CurrentSpawn, Quaternion.identity); // Repsawns player at the current spawn
			}
		} else if (currentLives == 0) { // Gives a Game Over if player has no lives
			SceneManager.LoadScene (5);
		}
	}

}
