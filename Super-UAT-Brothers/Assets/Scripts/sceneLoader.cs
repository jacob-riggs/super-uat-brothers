﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneLoader : MonoBehaviour {

	// Update is called once per frame
	public void SceneLoad (int scene) { // Takes a scene in the editor and loads it
		SceneManager.LoadScene (scene);
	}
}
