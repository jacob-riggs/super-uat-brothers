﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class nextLevel : MonoBehaviour {

	public int scene;

	void OnTriggerEnter2D(Collider2D other){ // After the player makes it to the end, it loads up the next scene
		if (other.tag == "Player") {
			SceneManager.LoadScene (scene);
		}
	}
		
}
